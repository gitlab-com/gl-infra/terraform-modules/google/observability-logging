{pkgs}: let
in
  pkgs.mkShell {
    name = "sre-observability/observabilty-logging-module";
    packages = with pkgs; [
      (mkTerraform {
        version = "1.5.4";
        hash = "sha256-MvN4gSJcPORD0wj6ixc3gUXPISGvAKSJPA6bS/SmDOY=";
        vendorHash = "sha256-lQgWNMBf+ioNxzAV7tnTQSIS840XdI9fg9duuwoK+U4=";
        patches = [./nix/patches/provider-path-0_15.patch];
      })

      terraform-docs
      tflint
      checkov
      glab
      shellcheck
      shfmt
      pre-commit
      alejandra
    ];
    buildInputs = [];
    shellHook = ''
      echo "Entering sre-observability/observabilty-logging-module shell"
      echo "Brought to you with love from the reliability:observabilty team <3"
    '';
  }
