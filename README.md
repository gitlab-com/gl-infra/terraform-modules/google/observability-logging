# Observability Logging Terraform Module

## What is this?

This module provisions components to ingest logs from Pubsub into Elasticsearch or Loki.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.5.4 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.7 |
| <a name="requirement_google-beta"></a> [google-beta](#requirement\_google-beta) | >= 4.7 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.7 |
| <a name="provider_google-beta"></a> [google-beta](#provider\_google-beta) | >= 4.7 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google-beta_google_cloud_run_v2_service.promtail_service](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_cloud_run_v2_service) | resource |
| [google_project_iam_member.observability_pubsub_subscriber](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.observability_secret_accessor](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_pubsub_subscription.promtail](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/pubsub_subscription) | resource |
| [google_secret_manager_secret.promtail_config](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/secret_manager_secret) | resource |
| [google_secret_manager_secret_version.promtail_config_version_data](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/secret_manager_secret_version) | resource |
| [google_service_account.observability_logging_account](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment"></a> [environment](#input\_environment) | n/a | `string` | `"gstg"` | no |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | The GCP project ID | `string` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | The GCP region to deploy in. | `string` | n/a | yes |
| <a name="input_loki_enabled"></a> [loki\_enabled](#input\_loki\_enabled) | Enable sending logs to Loki? | `bool` | `true` | no |
| <a name="input_loki_endpoint"></a> [loki\_endpoint](#input\_loki\_endpoint) | The loki endpoint to send logs too, you can usually leave this as the default. | `string` | `"https://loki-gateway.ops.gke.gitlab.net/loki/api/v1/push"` | no |
| <a name="input_loki_tenant_password"></a> [loki\_tenant\_password](#input\_loki\_tenant\_password) | The loki tenant password | `string` | n/a | yes |
| <a name="input_loki_tenant_username"></a> [loki\_tenant\_username](#input\_loki\_tenant\_username) | The loki tenant username | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | A unique name or id to base resource names off | `string` | n/a | yes |
| <a name="input_pubsub_queue_name"></a> [pubsub\_queue\_name](#input\_pubsub\_queue\_name) | The name of a pubsub queue to consume messages from | `string` | n/a | yes |
| <a name="input_stage"></a> [stage](#input\_stage) | n/a | `string` | `"main"` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).

## Getting Started

This section describes how a user can use the `Loki` module to send logs to the platform. This whole guide assumes that you provision your infrastructure using terraform.

1. First you need to make sure that the service account used by terraform has the `Logging Admin` role.

2. Before using the `Loki` module we need to have a `sink` that will send the logs to a pubsub topic. You can provision the following resources:

    ```terraform

    resource "google_pubsub_topic" "loki_observability_logs" {

    name   = var.loki_observability_logs_topic

    labels = var.labels

    }

    resource "google_logging_project_sink" "loki-log-sink" {

    name = "loki-log-sink"

    destination = "pubsub.googleapis.com/projects/${var.project_id}/topics/${google_pubsub_topic.loki_observability_logs.name}"

    # Use a unique writer (creates a unique service account used for writing)

    unique_writer_identity = true

    }

    resource "google_project_iam_binding" "publish" {

    project = var.project_id

    role    = "roles/pubsub.publisher"

    members = [

        google_logging_project_sink.loki-log-sink.writer_identity,

    ]

    }

    ```

3. Then provision the `Loki` module. Be aware that the loki tenant username, password and name variables values need to be provided by the Reliability team. `loki_tenant_password` needs to be treated as a secret.

    ```terraform

    module "loki" {

    source               = "git::https://gitlab.com/gitlab-com/gl-infra/terraform-modules/google/observability-logging"

    gcp_project_id       = var.project_id

    gcp_region           = var.region

    loki_tenant_password = var.loki_tenant_password

    loki_tenant_username = var.loki_tenant_username

    name                 = var.loki_name

    pubsub_queue_name    = google_pubsub_topic.loki_observability_logs.name

    environment          = var.env

    }

    ```

    Do not forget to declare `loki_tenant_password` as a sensitive variable:

    ```terraform

    variable "loki_tenant_password" {

    type        = string

    description = "The loki tenant password"

    sensitive   = true

    }

    ```

4. After step 3 you have provisioned all the required infrastructure to send logs to Loki. As a verification step go to the subscriptions page in GCP and check the metrics of the `pull-promtail-<loki_name>` subscription. You should see that messages are delivered correctly.

5. Finally you can view your data in the Grafana dashboard.

   1. Go to https://dashboards.gitlab.net/ (Grafana)

   2. On the top right press `+` and select `New Dashboard`.

   3. Select `Add Visualization`

   4. In the data source you should be able to find and select `<loki_name>` . Please notice that it might take a couple of meeting for a new data source to pop in the list.

   5. In the query write `{env="YOUR_ENV"}` , which means select all logs that have the `YOUR_ENV` env label.

   6. Then you can select Switch to table to see the logs.
