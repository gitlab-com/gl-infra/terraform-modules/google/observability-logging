resource "google_secret_manager_secret" "promtail_config" {
  count     = try(var.loki_enabled ? 1 : 0, 0)
  secret_id = "${var.name}-promtail-config"
  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_version" "promtail_config_version_data" {
  count  = try(var.loki_enabled ? 1 : 0, 0)
  secret = google_secret_manager_secret.promtail_config[0].name
  secret_data = templatefile("${path.module}/templates/promtail.yml.tftpl", {
    environment = var.environment,
    stage       = var.stage,
    region      = var.gcp_region,

    loki_endpoint           = var.loki_endpoint
    loki_tenant_username    = var.loki_tenant_username
    loki_tenant_password    = var.loki_tenant_password
    gcp_project_id          = var.gcp_project_id
    gcp_pubsub_subscription = google_pubsub_subscription.promtail.name
  })
}

resource "google_cloud_run_v2_service" "promtail_service" {
  count    = try(var.loki_enabled ? 1 : 0, 0)
  provider = google-beta
  name     = "promtail-${var.name}"
  location = var.gcp_region
  project  = var.gcp_project_id
  ingress  = "INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER"

  template {
    service_account = google_service_account.observability_logging_account.email
    containers {
      image = "docker.io/grafana/promtail:2.7.4"
      name  = "promtail"
      args  = ["-config.file=/secrets/${google_secret_manager_secret.promtail_config[0].secret_id}"]
      volume_mounts {
        name       = "config"
        mount_path = "/secrets"
      }
    }
    volumes {
      name = "config"
      secret {
        secret = google_secret_manager_secret.promtail_config[0].secret_id
      }
    }
  }
}
