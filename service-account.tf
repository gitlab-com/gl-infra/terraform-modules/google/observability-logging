resource "google_service_account" "observability_logging_account" {
  account_id   = "${var.name}-obs-acc"
  display_name = "Observability Logging Account"
}

resource "google_project_iam_member" "observability_pubsub_subscriber" {
  project = var.gcp_project_id
  role    = "roles/pubsub.subscriber"
  member  = "serviceAccount:${google_service_account.observability_logging_account.email}"
}

resource "google_project_iam_member" "observability_secret_accessor" {
  project = var.gcp_project_id
  role    = "roles/secretmanager.secretAccessor"
  member  = "serviceAccount:${google_service_account.observability_logging_account.email}"
}

resource "google_pubsub_subscription" "promtail" {
  name  = "pull-promtail-${var.name}"
  topic = var.pubsub_queue_name
}