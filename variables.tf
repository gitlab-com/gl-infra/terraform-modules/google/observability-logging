variable "pubsub_queue_name" {
  type        = string
  description = "The name of a pubsub queue to consume messages from"
}

variable "name" {
  type        = string
  description = "A unique name or id to base resource names off"
  validation {
    condition     = can(regex("[a-z]([-a-z0-9]*[a-z0-9])", var.name)) && length(var.name) <= 23 && length(var.name) >= 6
    error_message = "The name must conform to regex: [a-z]([-a-z0-9]*[a-z0-9]) and be <=23 and >6 characters in length."
  }
}

variable "loki_enabled" {
  type        = bool
  default     = true
  description = "Enable sending logs to Loki?"
}
variable "loki_endpoint" {
  type        = string
  default     = "https://loki-gateway.ops.gke.gitlab.net/loki/api/v1/push"
  description = "The loki endpoint to send logs too, you can usually leave this as the default."
}
variable "loki_tenant_username" {
  type        = string
  description = "The loki tenant username"
}
variable "loki_tenant_password" {
  type        = string
  description = "The loki tenant password"
  sensitive   = true
}

#variable "elasticsearch_enabled" {
#  type        = bool
#  default     = false
#  description = "Enable sending logs to Elasticsearch?"
#}

variable "gcp_project_id" {
  type        = string
  description = "The GCP project ID"
}
variable "gcp_region" {
  type        = string
  description = "The GCP region to deploy in."
}

variable "stage" {
  type    = string
  default = "main"
}
variable "environment" {
  type    = string
  default = "gstg"
}
